###resource group name
variable "resource_group_name" {
  description = "Resource Group Name"
  type = string #string number list
  default = "rg-default"
  ##where we will define multiple. like creating subnet we will use list
}

variable "resource_group_location" {
description = "Resource Group location"
  type = string #string number list
  default = "eastus"
}

variable "business_division" {
  type = string
  default = "sap"
}

variable "environment" {
  type = string
  default = "dev"
}