resource "null_resource" "null_copy_ssh" {
  depends_on = [azurerm_linux_virtual_machine.bastion_host_linuxvm]
  ###connection block for connecting to azure vm instance bastion instance
  connection {
    type = "ssh"
    host = azurerm_linux_virtual_machine.bastion_host_linuxvm.public_ip_address 
    user = azurerm_linux_virtual_machine.bastion_host_linuxvm.admin_username 
    private_key = file("${path.module}/ssh-keys/terraform.pem")
  }

  # file provisioned to copy the terraform.pem file inside your bastion host
  provisioner "file" {
    source = "ssh-keys/terraform.pem"
    destination = "/tmp/terraform.pem"
  }
  provisioner "remote-exec" {
    inline = [
        "sudo chmod 400 /tmp/terraform.pem"
    ]
  }
}